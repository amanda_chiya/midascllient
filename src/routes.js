/* eslint-disable react/display-name */
/* eslint-disable import/no-named-as-default */
/* eslint-disable import/no-named-as-default-member */
import React from 'react'
import PropTypes from 'prop-types'
import { Route, Switch, Redirect } from 'react-router-dom'

// Layouts
import BasicLayout from 'shared/components/layouts/BasicLayout'
import EmptyLayout from 'shared/components/layouts/EmptyLayout'
import PrivateRoute from 'shared/components/Authorization/PrivateRoute'
import InvoiceForm from 'modules/invoice'
import Wallet from 'modules/wallet'
import Profile from 'modules/profile'
import Voucher from 'modules/voucher'
import Contact from 'modules/contact'

import Dashboard from './modules/home'

// Login
import UserLogin from './account/components/loginContainer'
import UserRegister from './account/components/registerContainer'
import ResetContainer from './account/components/resetContainer'

// logout
import { logOut } from 'shared/state/actions'

// Confirm

import Confirm from 'modules/confirm'

// Exception
import NotFoundPage from './shared/components/exceptions/404'
import invoiceConfirm from 'modules/invoice-confirm'

const Wrapper = ({ wrapper: Wrapped, component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props => (
      <Wrapped>
        <Component {...props} />
      </Wrapped>
    )}
  />
)

const form = (Form, type, options) => props => (
  <Form {...props} formType={type} {...options} />
)

const Routes = ({ history }) => (
  <Switch history={history}>
    {/* Base */}
    <Redirect exact from="/" to="/login" />

    {/* Login */}
    <Wrapper exact path="/login" wrapper={EmptyLayout} component={UserLogin} />
    <Wrapper
      exact
      path="/register"
      wrapper={EmptyLayout}
      component={UserRegister}
    />
     <Wrapper
      exact
      path="/reset"
      wrapper={EmptyLayout}
      component={ResetContainer}
    />
    <Wrapper
      exact
      path="/confirm"
      wrapper={EmptyLayout}
      component={Confirm}
    />
    <PrivateRoute
      exact
      path="/invoice-confirm"
      wrapper={EmptyLayout}
      component={invoiceConfirm}
    />
    <PrivateRoute
      exact
      path="/dashboard"
      wrapper={BasicLayout}
      component={Dashboard}
    />
    <PrivateRoute exact path="/profile" wrapper={BasicLayout} component={Profile} />
    <PrivateRoute
      exact
      path="/invoice"
      wrapper={BasicLayout}
      component={InvoiceForm}
    />
    <PrivateRoute
      exact
      path="/voucher"
      wrapper={BasicLayout}
      component={Voucher}
    />
    <PrivateRoute
      exact
      path="/contact"
      wrapper={BasicLayout}
      component={Contact}
    />
    <PrivateRoute exact path="/wallet" wrapper={BasicLayout} component={Wallet} />

    <Wrapper wrapper={EmptyLayout} component={NotFoundPage} />
  </Switch>
)

Routes.propTypes = {
  history: PropTypes.object.isRequired
}

Wrapper.propTypes = {
  wrapper: PropTypes.oneOfType([PropTypes.element, PropTypes.func]).isRequired,
  component: PropTypes.oneOfType([PropTypes.element, PropTypes.func]).isRequired
}

export default Routes