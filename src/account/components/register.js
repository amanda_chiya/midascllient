/* eslint-disable import/no-mutable-exports */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import React from 'react'
import PropTypes from 'prop-types'
import {
  Card,
  Col,
  Row,
  Form,
  Icon,
  Input,
  Button,
  Checkbox,
  Select
} from 'antd'
import MainFooter from 'shared/components/layouts/MainFooter'
import { register } from 'account/state/accountActions'
import { Link } from 'react-router-dom'
import logo from 'shared/assets/logo.png'

const { Option } = Select

let UserRegister = ({
  submitting,
  regData,
  form: { getFieldDecorator, validateFields },
  register,
  push
}) => {
  const handleSubmit = e => {
    e.preventDefault()
    validateFields((err, values) => {
      if (!err) {
        register(values)
      }
    })
  }

  const dump = regData
    ? Object.entries(regData).map(data => (
        <Option key={data[0]} value={data[0]}>
          {data[1]}
        </Option>
      ))
    : {}

  return (
    <div className="auth-wrapper">
      <div className="auth-content">
        <Row gutter={16}>
          <Col span={24} className="text-center mb-5">
            <img src={logo} alt="Exide" />
          </Col>
          <Col span={24}>
            <Card ordered={false}>
              <Form onSubmit={handleSubmit} className="login-form">
                <Form.Item>
                  {getFieldDecorator &&
                    getFieldDecorator('name', {
                      rules: [
                        { required: true, message: 'Please input your name!' }
                      ]
                    })(
                      <Input
                     
                        placeholder="Name"
                        size="large"
                        className="mb-3"
                      />
                    )}
                </Form.Item>
                <Form.Item>
                  {getFieldDecorator &&
                    getFieldDecorator('surname', {
                      rules: [
                        {
                          required: true,
                          message: 'Please input your Surname!'
                        }
                      ]
                    })(
                      <Input
                  
                        type="surname"
                        placeholder="Surname"
                        size="large"
                        className="mb-3"
                      />
                    )}
                </Form.Item>

                <Form.Item>
                  {getFieldDecorator &&
                    getFieldDecorator('cellphone', {
                      rules: [
                        {
                          required: true,
                          message: 'Please input your Cellphone!'
                        }
                      ]
                    })(
                      <Input
                   
                        type="text"
                        placeholder="Mobile No."
                        size="large"
                        className="mb-3"
                      />
                    )}
                </Form.Item>
                {/* <Form.Item>
                  {getFieldDecorator &&
                    getFieldDecorator('password', {
                      rules: [
                        {
                          required: true,
                          message: 'Please input your Cellphone!'
                        }
                      ]
                    })(
                      <Input
               
                        type="password"
                        placeholder="password"
                        size="large"
                        className="mb-3"
                      />
                    )}
                </Form.Item> */}

                <Form.Item>
                  {getFieldDecorator &&
                    getFieldDecorator('branch_id', {
                      rules: [
                        {
                          required: true,
                          message: 'Please select your branch!'
                        }
                      ]
                    })(
                      <Select
             
                        style={{ width: 300 }}
                        placeholder="Select branch"
                        size="large"
                        className="mb-3"
                      >
                        {dump}
                      </Select>
                    )}
                </Form.Item>

                <Form.Item>
                  <Button
                    type="primary"
                    htmlType="submit"
                    block
                    className="login-submit"
                    size="large"
                    loading={submitting}
                  >
                    Register
                  </Button>
                </Form.Item>
                
              </Form>
            </Card>
          </Col>
          <Col span={24} className="text-center mt-5">
          <a style={{ height: '15' }} href="/login"><span  style={{ color: '#fff ',fontSize: '17px',fontWeight: '500', lineHeight: '1', textDecoration: 'underline' }} type="link">Registered? Login</span></a>
          </Col>
        </Row>
      </div>
    </div>
  )
}

UserRegister.defaultProps = {
  submitting: false
}

UserRegister.defaultProps = {
  push: () => {}
}

UserRegister.propTypes = {
  submitting: PropTypes.bool,
  form: PropTypes.object.isRequired,
  push: PropTypes.func
}

UserRegister = Form.create({ name: 'register' })(UserRegister)

export default UserRegister
