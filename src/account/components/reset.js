/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable import/no-mutable-exports */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import React from 'react'
import PropTypes from 'prop-types'
import { Card, Row, Col, Form, Icon, Input, Button, Checkbox } from 'antd'
import MainFooter from 'shared/components/layouts/MainFooter'
import { Link } from 'react-router-dom'
import logo from 'shared/assets/logo.png'

let PasswordReset = ({
  submitting,
  form: { getFieldDecorator, validateFields },
  createReset,
  push
}) => {
  const handleSubmit = e => {
    e.preventDefault()
    validateFields((err, values) => {
      if (!err) {
        createReset(values)
      }
    })
  }

  return (
    <div className="auth-wrapper">
      <div className="auth-content">
        <Row gutter={16}>
          <Col span={24} className="text-center mb-5">
            <img src={logo} alt="Exide" />
          </Col>
          <Col span={24}>
            <Card title="Reset Password" bordered={false}>
              <Form onSubmit={handleSubmit} className="login-form">
                <Form.Item>
                  {getFieldDecorator &&
                    getFieldDecorator('email', {
                      rules: [
                        {
                          required: true,
                          message: 'Please input your cellphone!'
                        }
                      ]
                    })(
                      <Input
                        prefix={
                          <Icon
                            type="user"
                            style={{ color: 'rgba(0,0,0,.25)' }}
                          />
                        }
                        size="large"
                        placeholder="Cellphone"
                      />
                    )}
                </Form.Item>

                <Form.Item>
                  <Button
                    type="primary"
                    htmlType="submit"
                    block
                    className="login-submit"
                    icon="login"
                    loading={submitting}
                  >
                    Reset
                  </Button>
                </Form.Item>
              </Form>
            </Card>
          </Col>
          <Col span={24} className="text-center mt-5">
            
            <Link to="/login"> <span style={{ color: '#fff ',fontSize: '17px',fontWeight: '500', lineHeight: '1', textDecoration: 'underline' }} type="link">Registered? Login</span></Link>
          </Col>
        </Row>
      </div>
    </div>
  )
}

PasswordReset.defaultProps = {
  submitting: false,
  performLogin: () => {}
}

PasswordReset.defaultProps = {
  push: () => {}
}

PasswordReset.propTypes = {
  submitting: PropTypes.bool,
  performLogin: PropTypes.func,
  form: PropTypes.object.isRequired,
  push: PropTypes.func
}

PasswordReset = Form.create({ name: 'login' })(PasswordReset)

export default PasswordReset