import React, { Component } from 'react'
import { connect } from 'react-redux'
import { push } from 'connected-react-router'
import * as actions from '../state/accountActions'
import RegisterUser from './register'

class UserRegisterContainer extends Component {
  componentDidMount() {
    this.props.branches()
  }

  render() {
    console.log(this.props)
    return <RegisterUser {...this.props} />
  }
}

const mapStateToProps = ({
  shared: {
    form: { submitting },
    item
  }
}) => ({
  submitting,
  regData: item.item
})

const mapDispatchToProps = { ...actions, push }

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserRegisterContainer)
