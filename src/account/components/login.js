/* eslint-disable import/no-mutable-exports */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import React from 'react'
import PropTypes from 'prop-types'
import { Card, Row, Col, Form, Icon, Input, Button, Checkbox } from 'antd'
import MainFooter from 'shared/components/layouts/MainFooter'
import { Link } from 'react-router-dom'
import logo from 'shared/assets/logo.png'

let UserLogin = ({
  submitting,
  form: { getFieldDecorator, validateFields },
  performLogin,
  push
}) => {
  const handleSubmit = e => {
    e.preventDefault()
    validateFields((err, values) => {
      if (!err) {
        performLogin(values)
      }
    })
  }

  return (
    <div className="auth-wrapper">
      <div className="auth-content">
        <Row gutter={16}>
        <Col span={24} className="text-center mb-5">
            <img src={logo} alt="Exide" />
          </Col>
          <Col span={24}>
            <Card title="Sign in" bordered={false}>
              <Form onSubmit={handleSubmit} className="login-form">
                <Form.Item>
                  {getFieldDecorator &&
                    getFieldDecorator('cellphone', {
                      rules: [
                        {
                          required: true,
                          message: 'Please input your cellphone!'
                        }
                      ]
                    })(
                      <Input
                     
                        size="large"
                        placeholder="Mobile No."
                        className="mb-3"
                      />
                    )}
                </Form.Item>
                <Form.Item>
                  {getFieldDecorator &&
                    getFieldDecorator('password', {
                      rules: [
                        {
                          required: true,
                          message: 'Please input your Password!'
                        }
                      ]
                    })(
                      <Input
                     
                        type="password"
                        size="large"
                        placeholder="Password"
                        className="mb-3"
                      />
                    )}
                </Form.Item>
                <Form.Item>
                  <Button
                    type="primary"
                    htmlType="submit"
                    block
                    size="large"
                    className="login-submit"
                    loading={submitting}
                  >
                    Login
                  </Button>
                </Form.Item>
              </Form>
            </Card>
          </Col>
          <Col span={24} className="text-center mt-5">
          <a style={{ height: '15' }} href="/register"><span style={{ color: '#fff ',fontSize: '17px',fontWeight: '500', lineHeight: '1', textDecoration: 'underline' }} type="link">Not registered? Register</span></a>
          </Col>
          <Col span={24} className="text-center mt-5">
             <Link to="/reset"><span style={{ color: '#fff ',fontSize: '17px',fontWeight: '500', lineHeight: '1', textDecoration: 'underline' }} type="link">Forgot Password? Reset</span></Link>
          </Col>
        </Row>
      </div>
    </div>
  )
}

UserLogin.defaultProps = {
  submitting: false,
  performLogin: () => {}
}

UserLogin.defaultProps = {
  push: () => {}
}

UserLogin.propTypes = {
  submitting: PropTypes.bool,
  performLogin: PropTypes.func,
  form: PropTypes.object.isRequired,
  push: PropTypes.func
}

UserLogin = Form.create({ name: 'login' })(UserLogin)

export default UserLogin
