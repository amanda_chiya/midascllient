import api from 'shared/api/request'
import { selectToken } from 'shared/state/actions'
import { push } from 'connected-react-router'

export const register = data => dispatch => {
  dispatch({ type: 'shared/form/SUBMITTING' })
  api
    .post('/register', { data })
    .then(({ message }) => {
      dispatch({
        type: 'shared/form/SUBMITTED'
      })
      return dispatch(push('/confirm'))
    })
    .catch(body => {
      dispatch({
        type: 'shared/form/SUBMITTED',
        messages: body
      })
    })
}

export const branches = () => dispatch => {
  dispatch({ type: 'shared/list/FETCHING' })
  api
    .get('/branches')
    .then(({ data }) => {
      console.log(data)
      dispatch({
        type: 'shared/item/FETCHED_ITEM',
        item: data
      })
    })
    .catch(error => {
      dispatch({ type: 'shared/list/FETCH_ERROR', messages: error })
    })
}
