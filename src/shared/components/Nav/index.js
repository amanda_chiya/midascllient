import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { Layout, Icon, Drawer, Button, Menu, Row, Col, PageHeader   } from 'antd'
import logo from 'shared/assets/logo.png'
import { getModules } from 'shared/static/modules'
import { logOut } from 'shared/state/actions'

const { Header, Sider } = Layout
const { SubMenu } = Menu
const modules = getModules('123')
const logout = logOut()

class Nav extends Component {
  constructor(props){
    super(props)
  }
  state = {
    current: 'mail',
    visible: false
  }

  showDrawer = () => {
    this.setState({
      visible: true,
    })
  }

  onClose = () => {
    this.setState({
      visible: false,
    })
  }

  onChildrenDrawerClose = () => {
    this.setState({
      childrenDrawer: false,
    })
  }

  render() {
    return (

      

      <nav className="menuBar">

<PageHeader
    style={{
      border: '1px solid rgb(235, 237, 240)',
    }}
    onBack={() => window.history.back()}
    title="Back"
    subTitle=""
  />
        <Header>

        <Row gutter={0}>

          <Col span={6}>
          <Button className="barsMenu border-0" type="primary" size="large" onClick={this.showDrawer}>
            <Icon type="align-left" />
          </Button>
          </Col>
          <Col span={12}>
          <div className="logo text-center">
          <img src={logo} alt="Exide" />
        </div>
          </Col>
          <Col span={6}></Col>
        </Row>
     </Header>
 
        
        <div className="menuCon">
          <div className="leftMenu" />
          <Drawer
            title="Navigation"
            placement="left"
            closable
            onClose={this.onClose}
            visible={this.state.visible}
          >
            {modules.length ? (
              <span>
                <Menu theme="light" mode="inline">
                  {modules.map(({ key, link, title, icon, sub }, idx) =>
                    sub && sub.length > 0 ? (
                      <SubMenu
                        key={`sub${idx}`}
                        title={
                          <span>
                            <Icon type={icon} />
                            <span>{title}</span>
                          </span>
                        }
                      >
                        {sub &&
                          sub.map((item, index) => (
                            <Menu.Item
                              key={`${item.title.replace(/\s/g, '-')}${index}`}
                              to="taps"
                              onClick={this.onClose}

                            >
                              <Icon type={item.icon} />
                              <span>
                                <Link to={item.link} className="sub-menu-row">
                                  {item.title}
                                </Link>
                              </span>
                            </Menu.Item>
                          ))}
                      </SubMenu>
                    ) : (
                      <Menu.Item key={`${key}${idx}`}>
                        <Icon type={icon} />
                        <span>
                          <Link to={link}>{title}</Link>
                        </span>
                      </Menu.Item>
                    )
                  )}
                  <Menu.Item key="logout">
                        <Icon type="logout" />
                        <span>
                          <Link onClick={logout}>Logout</Link>
                        </span>
                      </Menu.Item>
                </Menu>
              </span>
            ) : null}
          </Drawer>
        </div>
      </nav>
    )
  }
}

export default Nav
