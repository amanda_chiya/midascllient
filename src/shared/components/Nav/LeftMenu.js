import React, { Component } from 'react';
import { Menu, Icon } from 'antd';
const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;

class LeftMenu extends Component {
  render() {
    return (
        {modules.length ? (
            <span>
              <Menu theme="light" mode="inline">
                {modules.map(({ key, link, title, icon, sub }, idx) =>
                  sub && sub.length > 0 ? (
                    <SubMenu
                      key={`sub${idx}`}
                      title={
                        <span>
                          <Icon type={icon} />
                          <span>{title}</span>
                        </span>
                      }
                    >
                      {sub &&
                        sub.map((item, index) => (
                          <Menu.Item key={`${item.title.replace(/\s/g, '-')}${index}`} to="veli">
                            <Icon type={item.icon} />
                            <span>
                              <Link to={item.link} className="sub-menu-row">
                                {item.title}
                              </Link>
                            </span>
                          </Menu.Item>
                        ))}
                    </SubMenu>
                  ) : (
                    <Menu.Item key={`${key}${idx}`}>
                      <Icon type={icon} />
                      <span>
                        <Link to={link}>{title}</Link>
                      </span>
                    </Menu.Item>
                  )
                )}
              </Menu>
            </span>


          ) : null}
    );
  }
}

export default LeftMenu;