import React from 'react'
import { Layout, Card, Row, Col } from 'antd'

const { Footer } = Layout
const MainFooter = () => {
  return (
    <Footer
      style={{
        textAlign: 'center'
      }}
    >

      <Row>

        <Col span={24}>

    <Card>
        <p><a href="https://www.exide.co.za/terms-conditions-exide-app/" target="_blank"> Terms and Conditions</a></p>

      </Card>

      </Col>

      </Row>

    </Footer>
  )
}

export default MainFooter
