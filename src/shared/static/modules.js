export const getModules = id =>
  !id
    ? []
    : [
        {
          title: 'Home',
          key: 'home',
          icon: 'home',
          link: '/dashboard'
        },
        {
          title: 'Wallet',
          key: 'wallet',
          icon: 'wallet',
          link: '/wallet'
        },
        {
          title: 'Add Invoice',
          key: 'addInvoice',
          icon: 'plus',
          link: '/invoice'
        },
        {
          title: 'Profile',
          key: 'profile',
          icon: 'user',
          link: '/profile'
        },
        {
          title: 'Contact Us',
          key: 'contactUs',
          icon: 'mail',
          link: '/contact'
        }
      ]
