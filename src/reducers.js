import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'
import shared from 'shared/state/sharedReducer'

export default history =>
  combineReducers({
    router: connectRouter(history),
    shared
  })
