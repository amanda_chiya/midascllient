import api from 'shared/api/request'
import { notification } from 'antd'
import { push } from 'connected-react-router'

export const getUserProfile = () => dispatch => {
  dispatch({ type: 'shared/list/FETCHING' })
  api
    .get('/user')
    .then(user => {
      dispatch({ type: 'shared/item/FETCHED_ITEM', item: user.data })
    })
    .catch(({ error }) => {
      dispatch({ type: 'shared/list/FETCH_ERROR' })
    })
}

export const createinvoice = (rData, file) => dispatch => {
  const quantity = rData.items
    .map(item => item.quantity)
    .reduce((a, b) => +a + +b, 0)

  const itemsD = rData.items.reduce((obj, i) => {
    return {
      ...obj,
      [i.items]: +i.quantity
    }
  }, {})

  const _data = {
    quantity,
    items: JSON.stringify(itemsD),
    receipt_number: rData.receipt_number,
    file
  }

  dispatch({ type: 'shared/form/SUBMITTING' })
  api
    .post(`/receipts`, { data: _data })
    .then(({ data, message }) => {
      if (data) {
        notification.success({
          message: `Receipt is created succesfully!`,
          description: message
        })
        dispatch(push('/invoice-confirm'))
      }
    })
    .catch(({ error }) => {
      dispatch({ type: 'shared/form/SUBMITTED', messages: error })
      notification.error({
        message: 'Error uploading receipt',
        description: error
      })
    })
}

export const fetchWallets = () => dispatch => {
  dispatch({ type: 'shared/list/FETCHING' })
  api
    .get(`/wallets`)
    .then(({ data }) => {
      console.log('wallets', data)
      dispatch({
        type: 'shared/item/FETCHED_ITEM',
        item: data
      })
    })
    .catch(({ error }) => {
      console.info('Search Batches error', error)
      dispatch({ type: 'shared/list/FETCH_ERROR', messages: error })
    })
}

export const fetchBatteryCodes = () => dispatch => {
  dispatch({ type: 'shared/list/FETCHING' })
  api
    .get(`/codes`)
    .then(({ data }) => {
      dispatch({
        type: 'shared/item/FETCHED_ITEM',
        item: data
      })
    })
    .catch(({ error }) => {
      console.info('Search Batches error', error)
      dispatch({ type: 'shared/list/FETCH_ERROR', messages: error })
    })
}

export const purchaseAir = value => dispatch => {
  dispatch({ type: 'shared/form/SUBMITTING' })
  api
    .post(`/purchase-airtime?Units=R${value}`)
    .then(({ data, message }) => {
      if (data) {
        dispatch({ type: 'shared/form/SUBMITTED', messages: message })
        notification.success({
          message: `Voucher Purchase`,
          description: message
        })
      }
    })
    .catch(({ error }) => {
      dispatch({ type: 'shared/form/SUBMITTED', messages: error })
    })
}

export const purchaseVoucher = value => dispatch => {
  dispatch({ type: 'shared/form/SUBMITTING' })
  const pred =
    value[0] === 'PINNED_AIRTIME' || 'PINLESS_DATA'
      ? `${value[2]}`
      : `R${value[2]}`
  api
    .get(`/purchase-voucher?Units=${pred}&Product=${value[0]}`)
    .then(({ data, message }) => {
      if (data) {
        notification.success({
          description: message,
          message
        })

        setTimeout(() => {
          // eslint-disable-next-line no-undef
          window.location.reload()
        }, 3000)
      }
    })
    .catch(({ error }) => {
      dispatch({ type: 'shared/form/SUBMITTED', messages: error })
    })
}
