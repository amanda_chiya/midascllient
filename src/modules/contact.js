/* eslint-disable react/prefer-stateless-function */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from 'react'
import { Typography, Statistic, Icon, Card, Col, Row, Progress, Alert } from 'antd'
import hero from 'shared/assets/Wallet_lockup.jpg'

const { Title } = Typography

class Contact extends Component {
  render() {
    return (
      <>

        <div className="content-wrapper-empty">
          {' '}
          <div className="dashboard">

          <Row className="mb-5">
          <Col span={24}>
            <Card >
             <p className="text-left" style={{ fontSize: '18px', color: '#000' }}>Contact details</p>
              <strong>Amanda Brock</strong><br/>
              <a href="mailto:amandab@gullanandgullan.com">amandab@gullanandgullan.com</a><br/>
              <a href="tel:0118876591">011 887 6591</a>
            </Card>
          </Col>
         
          </Row>
          

            <Card
                style={{ width: '100%' }}
                cover={
                  <img
                    alt="Exide"
                    src={hero}
                  />
                }

              >

              </Card>
          </div>
        </div>
      </>
    )
  }
}

export default Contact
