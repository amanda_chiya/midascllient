/* eslint-disable react/prop-types */
/* eslint-disable react/display-name */
/* eslint-disable import/no-mutable-exports */
import React, { Component, useState, useEffect } from 'react'
import { connect } from 'react-redux'
import {
  Spin,
  Typography,
  Form,
  Input,
  Button,
  Select,
  Card,
  Row,
  Col,
  Upload,
  Icon,
  message
} from 'antd'
import PropTypes from 'prop-types'
import Webcam from 'react-webcam'
import * as actions from './state/index'
import DynamicFields from './DynamicFields'

const { TextArea } = Input

const { Title } = Typography
const { Option } = Select

let isSubmitting = false
function setSubmittState(){
  isSubmitting = true
}
let InvoiceForm = ({
  form,
  createinvoice,
  fetchBatteryCodes,
  codes
}) => {
  const [imageUrl, handleImgUrl] = useState(null)
  const [encodedImageUrl, handleEncodedImgUrl] = useState('')
  const [loading, handleLoading] = useState(false)
  const [isButtonLoading, setIsButtonLoading, submitting] = React.useState(false)
  useEffect(() => {
    fetchBatteryCodes()
  }, [fetchBatteryCodes])

  const handleSubmit = e => {
    e.preventDefault()
    form.validateFields((err, values) => {
      if (err) {
        console.log('Received values of form: ', values)
        return
      }

      createinvoice(values, encodedImageUrl)
    })
  }

  const getBase64 = (img, callback) => {
    const reader = new FileReader()
    reader.addEventListener('load', () => callback(reader.result))
    reader.readAsDataURL(img)
  }

  const beforeUpload = file => {
    const isJpgOrPng = file.type === 'image/jpeg'
    if (!isJpgOrPng) {
      message.error('You can only upload JPG file!')
    }
    const isLt2M = file.size / 1024 / 1024 < 4
    if (!isLt2M) {
      message.error('Image must smaller than 4MB!')
    }

    getBase64(file, imageUri => {
      handleEncodedImgUrl(imageUri)
    })

    handleImgUrl(file)

    return false
  }

  const uploadButton = (
    <div>
      <Icon type={loading ? 'loading' : 'plus'} />
      <div className="ant-upload-text">Use your phone camera to capture your receipt and upload here. </div>
    </div>
  )

  const dump = codes
    ? Object.entries(codes).map(data => (
        <Option key={data[0]} value={data[1]}>
          {data[1]}
        </Option>
      ))
    : []

  return (
    <>
      <Title level={3} className="heading-wrapper">
        Invoice
        <Title type="secondary" level={4}>
          Add Invoice
        </Title>
      </Title>

      <Card className="m-3">
        <Form
          layout="vertical"
          onSubmit={handleSubmit}
          className="ant-advanced-search-form"
        >
          <Form.Item
            help="Please enter invoice number"
            style={{ display: 'inline-block', width: '100%', margin: 0 }}
          >
            {form.getFieldDecorator('receipt_number', {
              rules: [
                {
                  required: true,
                  message: 'Please input a valid invoice number!'
                }
              ]
            })(<Input style={{ width: '100%' }} />)}
          </Form.Item>

          <DynamicFields
            {...form}
            name="items"
            fields={[
              {
                name: 'items',
                field: () => (
                  <Select
                    style={{ width: '100%', margin: 0 }}
                    placeholder="Select battery code"
                    size="large"
                  >
                    {dump}
                  </Select>
                )
              },
              {
                name: 'quantity',
                field: () => <Input placeholder="Quantity" size="large" />
              }
            ]}
          />

          <Upload
            name="sample"
            listType="picture-card"
            className="avatar-uploader mt-3"
            showUploadList={false}
            beforeUpload={beforeUpload}
          >
            {encodedImageUrl ? (
              <img src={encodedImageUrl} alt="test" style={{ width: '100%' }} />
            ) : (
              uploadButton
            )}
          </Upload>

          <Form.Item>
            <Button
              type="primary"
              htmlType="submit"
              // style={{ display: 'inline-block', margin: 10 }}
              block
              size="large"
              className="login-submit"
              // hidden={isSubmitting}
              onClick={setSubmittState}
            >
             {isSubmitting ? <Spin className="submitting-color" /> : "Submit Receipt"}
            </Button>
            
          </Form.Item>
        </Form>
      </Card>
      <Card className="m-3" >
        <p className="text-left" style={{ fontSize: '18px', color: '#000' }}>Please note</p>
        By registering all your battery sales on the app, you’re well on the way to becoming an Exide-ing legend. However, we need to measure your sales on a monthly basis.<br/>
When uploading your invoices, please ensure they are for the present month and not from months that have passed by. This means all Exide battery sales from November, must be uploaded in November. Invoices from previous months will NOT be registered.

      </Card>
    </>
  )
}

InvoiceForm = Form.create({ name: 'invoice' })(InvoiceForm)

const mapStateToProps = ({
  shared: {
    item: { item }
  }
}) => ({
  codes: item
})

const mapDispatchToProps = actions

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(InvoiceForm)