/* eslint-disable react/jsx-one-expression-per-line */
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Button, Icon, Card, Col, Row, Progress, Alert } from 'antd'
import * as action from './state/index'

class Wallet extends Component {
  componentDidMount() {
    const { getUserProfile } = this.props
    getUserProfile()
  }

  render() {
    console.log('current State', this.props)
    const { wallet, vouchers } = this.props
    return (
      <>
        <Card
          className="m-3"
          actions={[
            <Button href="./invoice" shape="round" size="small" icon="plus">Add invoice</Button>,

            <Button href="./voucher" shape="round" size="small">Redeem</Button>
          ]}
        >
           <p className="text-center">Wallet balance</p>
          <h1 className="text-center">R{wallet?.balance}</h1>

          <div className="text-center">

            <Progress percent={wallet?.totalAmountEarned} successPercent={wallet?.pendingRewards} format={percent => `R${wallet?.balance}`} />
          </div>
        </Card>

        <Card title="Vouchers" className="m-3">
          <Row>
            <Col span={6}>
              <h1>{vouchers?.length}</h1>
            </Col>
            <Col span={10}>
              VOUCHERS
              <br />
              Redeemed
            </Col>
            <Col span={8}>
              <Button href="./profile" type="dashed" shape="round" size="small">
                View
              </Button>
            </Col>
          </Row>
        </Card>

        <Card title="History" className="m-3">
          <Row gutter="16">
            <Col span={12}>
              <h3 className="text-center mb-0">R{wallet?.totalAmountEarned}</h3>
              <p className="text-center">Earned</p>
            </Col>
            <Col span={12}>
              <h3 className="text-center mb-0">R{wallet?.totalAmountRedeemed}</h3>
              <p className="text-center">Redeemed</p>
            </Col>
          </Row>
        </Card>
      </>
    )
  }
}

const mapStateToProps = ({
  shared: {
    item: { item }
  }
}) => ({
  vouchers: item.vouchers,
  wallet: item.wallet
})

const mapDispatchToProps = action

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Wallet)