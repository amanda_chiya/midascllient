/* eslint-disable react/prefer-stateless-function */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Typography, Statistic, Icon, Card, Col, Row, Progress, Alert } from 'antd'
import * as action from './state/index'
import hero from 'shared/assets/Wallet_lockup.jpg'


const { Title } = Typography

class Dashboard extends Component {

  componentDidMount() {
    const { getUserProfile } = this.props
    getUserProfile()
  }

  render() {
    const { wallet } = this.props
    return (
      <>



        <div className="content-wrapper-empty">
          {' '}
          <div className="dashboard">

          <Row className="row mb-4 mt-4">
							<div className="col-xl-4 col-md-6">
								<div className="card user-card user-card-3">

                   <div className="card-body bg-light">
										<div className="row text-center">
											<div className="col">
												<h6 className="mb-1">{wallet?.balance}</h6>
												<p className="mb-0">Wallet balance</p>
											</div>
											<div className="col">
												<h6 className="mb-1">{wallet?.pendingRewards}</h6>
												<p className="mb-0">Pending rewards</p>
											</div>
											<div className="col">
												<h6 className="mb-1"><Icon type="plus" /></h6>
												<p className="mb-0"><a href="./invoice">Add invoice</a></p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</Row>

          
          <Row>
          <Col span={24}>
           
          </Col>
          </Row>
 
            <Card
                style={{ width: '100%' }}
                cover={
                  <img
                    alt="Exide"
                    src={hero}
                  />
                }

              >

              </Card>
          </div>
        </div>
      </>
    )
  }
}

const mapStateToProps = ({
  shared: {
    item: { item }
  }
}) => ({
  wallet: item.wallet
})

const mapDispatchToProps = action

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Dashboard)
