import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Result, Button } from 'antd'

class Confirm extends Component {

render() {

return (
  <Result
    status="success"
    title="Congrats! You’ve successfully signed up to the Exide-ing app."
    subTitle="Remember, to earn and redeem rewards, you need to sell at least ONE Exide battery a month."
  />
)

}

}

export default connect(

)(Confirm)