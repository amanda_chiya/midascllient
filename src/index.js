/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import 'bootstrap/dist/css/bootstrap.min.css'
import { Provider } from 'react-redux'
import { ConnectedRouter as Router } from 'connected-react-router'
import configureStore, { history } from './store'
import  * as serviceWorker  from './serviceWorker'
import Routes from './routes'
import './index.less'

const store = configureStore()

ReactDOM.render(
  <Provider store={store}>
    <Router history={history}>
      <Routes history={history} />
    </Router>
  </Provider>,
  document.getElementById('root')
)

serviceWorker.register({
  onUpdate: registration => {
    const waitingServiceWorker = registration.waiting

    if (waitingServiceWorker) {
      waitingServiceWorker.addEventListener("statechange", event => {
        if (event.target.state === "activated") {
          window.location.reload()
        }
      });
      waitingServiceWorker.postMessage({ type: "SKIP_WAITING" })
    }
  }
})